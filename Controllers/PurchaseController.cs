﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BABC_Logistics_core.GIS.DTO;
using BABC_Logistics_core.GIS.Model;
using BABC_Logistics_core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BABC_Logistics_core.Controllers
{
    public class PurchaseController : Controller
    {
        static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private PurchaseModel mPU;
        private UserModel mUM;
        private SupplierModel mSM;
        private BaseDataModel mBDM;
        //private PrintingModel mPM;
        private string mGeneralIndexSearchCount;

        public PurchaseController(IConfiguration config)
        {
            mPU = new PurchaseModel(config);
            mUM = new UserModel(config);
            mSM = new SupplierModel(config);
            mBDM = new BaseDataModel(config);
            //PrintingModel mPM = new PrintingModel(config);
            string mGeneralIndexSearchCount;


        mGeneralIndexSearchCount = "5000";
        }

        // GET: Purchase
        public IActionResult Index(string search_value, string search_type)
        {
            List<PurchaseOrderHead> lst = new List<PurchaseOrderHead>();

            if (string.IsNullOrEmpty(search_value))
            {
                return View();
            }

            if (search_type.Equals("Datum"))
            {
                lst = mPU.GetOrderByIdxList("1", "*", "LDT>=" + search_value, false, 20, true);
            }
            else if (search_type.Equals("Leverantör"))
            {
                lst = mPU.GetOrderByIdxList("2", search_value, "LEF<=4", false, 50, false);
            }
            else if (search_type.Equals("Säljarkod"))
            {
                lst = mPU.GetOrderByIdxList("4", search_value, "*", false, 50, false);
            }

            List<PurchaseOrderHead> sortedList = lst.OrderByDescending(o => o.OrderNo).ToList();

            return View(sortedList);
        }

        public IActionResult PickList(PurchaseListVM pVM)
        {
            List<PurchaseOrderHead> lst = new List<PurchaseOrderHead>();
            StringBuilder filter = new StringBuilder();


            if (!string.IsNullOrEmpty(pVM.search_supplierName))
            {
                filter.Append("LEF!=5;OTY==1");
                lst = mPU.GetOrderPickList("2", pVM.search_supplierName, "0", filter.ToString(), "A", true, false, 50, true);
            }
            else if (!string.IsNullOrEmpty(pVM.search_order))
            {
                filter.Append("LEF!=5;OTY==1");
                lst = mPU.GetOrderPickList("1", pVM.search_order, "0", filter.ToString(), "A", true, false, 50, true);
            }
            else
            {
                filter.Append("LEF!=5;OTY==1");
                lst = mPU.GetOrderPickList("1", "*", "0", filter.ToString(), "A", true, false, 50, true);
            }

            foreach (PurchaseOrderHead oh in lst)
            {
                oh.ConnectedOrder = false;
                foreach (OrderRow row in oh.OrderRows)
                {
                    foreach (OrderRowText txt in row.Textrows)
                    {
                        if (txt.PickListState.Equals("K"))
                            oh.ConnectedOrder = true;
                    }
                }
            }
            List<PurchaseOrderHead> sortedList = lst.OrderByDescending(o => o.Date)
                .ThenBy(o => o.OrderNo)
                .ToList();
            pVM.OrderHeads = sortedList;

            return View(pVM);
        }

        public IActionResult Details(string id)
        {
            List<PurchaseOrderHead> lst = mPU.GetOrderPickList("1", id, "0", "*", "A", true, true, 1, false);
            PurchaseOrderHead oh = null;

            if (lst.Count > 0)
                oh = lst[0];
            else
                oh = null;

            if (oh != null)
            {
                BaseTable ls = mBDM.GetBaseTable("33", oh.DeliverWay);
                if (ls != null)
                    oh.DeliverWay = ls.Description;


            }

            if (oh.OrderRows == null)
                oh.OrderRows = new List<OrderRow>();

            // Remove all rows without product
            oh.OrderRows.RemoveAll(o => string.IsNullOrEmpty(o.ProductNo));

            var sortedRows = (from OrderRow r in oh.OrderRows
                              orderby r.RowNo ascending
                              select r).ToList();

            oh.OrderRows = sortedRows;

            return View(oh);
        }

        public IActionResult PickOrder(string id, string sign)
        {
            List<PurchaseOrderHead> lst = mPU.GetOrderPickList("1", id, "0", "*", "A", true, true, 1, false);
            PurchaseOrderHead oh = null;
            PurchaseOrderViewModel ovm = new PurchaseOrderViewModel();

            if (lst.Count > 0)
            {
                ovm.Order = lst[0];

                if (!ovm.Order.Equals("5") || (sign.Equals("rockon")))
                {
                    ovm.Order.PurchaseState = "5";

                    BaseTable ls = mBDM.GetBaseTable("33", ovm.Order.DeliverWay);
                    if (ls != null)
                        ovm.Order.DeliverWay = ls.Description;

                    BaseTable lv = mBDM.GetBaseTable("32", ovm.Order.DeliverTerms);

                    // Remove all rows without product
                    ovm.Order.OrderRows.RemoveAll(o => string.IsNullOrEmpty(o.ProductNo));

                    try
                    {
                        var sortedRows = (from OrderRow r in ovm.Order.OrderRows
                                          orderby r.Product.WarehouseNo ascending
                                          select r).ToList();

                        ovm.Order.OrderRows = sortedRows;
                    }
                    catch (Exception e) { }
                    //hämta kundfält för PS info
                    Supplier customer = mSM.GetSupplierById(ovm.Order.SupplierNo);

                    ViewBag.Customer = customer;
                    //oh.isPrioOrder = CheckPrioOrder(oh.OrderNo);

                    ovm.SelectableProducts = new List<Product>();

                }
                else
                {
                    return RedirectToAction("OrderIsAlreadyUnderPicking", "Order", new { order = ovm.Order.OrderNo, sign = ovm.Order.Season, forwardpath = "/Order/PickOrder/" + ovm.Order.OrderNo + "/rockon", backpath = "/Order/PickList" });
                }
            }
            else
                ovm.Order = null;

            return View(ovm);
        }

        public string DeliverOrderRow(string order, string row, string state, string amount)
        {
            List<PurchaseDeliverRowParam> deliver = new List<PurchaseDeliverRowParam>();
            OrderRow or = mPU.GetOrderRow(order, row);
            string decSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            decimal? dAmount = null;

            try
            {
                if (decSep == ".")
                {
                    dAmount = decimal.Parse(amount.Replace(",", "."));
                }
                else
                {
                    dAmount = decimal.Parse(amount.Replace(".", ","));
                }
            }
            catch { }

            try
            {
                if (or != null && dAmount.HasValue)
                {
                    if (dAmount.Value != 0)
                    {
                        if (or.DeliverState.CompareTo("5") < 0)
                        {
                            PurchaseDeliverRowParam dorp = new PurchaseDeliverRowParam();

                            dorp.OrderNo = or.OrderNo;
                            dorp.RowNo = or.RowNo;
                            dorp.WarehouseNo = or.WarehouseNo;
                            dorp.DeliverState = "5";
                            dorp.Amount = dAmount.Value;
                            dorp.DeliverNoteNo = "";

                            deliver.Add(dorp);
                        }

                        DeliverResult result = mPU.Deliver(deliver);

                        return result.DeliverNo;
                    }
                    else
                        return "FAILED";
                }
                else
                {
                    return "FAILED";
                }
            }
            catch
            {
                return "FAILED";
            }

        }

        public string DeliverOrderRows(string order, List<PurchaseDeliverRowParam> rows, string state)
        {
            string decSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            try
            {
                foreach (var row in rows)
                {
                    if (decSep == ".")
                    {
                        row.Amount = decimal.Parse(row.Amount.ToString().Replace(",", "."));
                    }
                    else
                    {
                        row.Amount = decimal.Parse(row.Amount.ToString().Replace(".", ","));
                    }

                    row.DeliverDate = "";
                    row.DeliverState = state;
                    row.OrderNo = order;
                    row.WarehouseNo = "";
                }

            }
            catch { }

            try
            {
                DeliverResult result = mPU.Deliver(rows);
                return result.DeliverNo;

            }
            catch
            {
                return "FAILED";
            }
        }


        public string BackDeliverOnOrderRows(string order, int[] rowIds)
        {
            try
            {
                List<BackDeliverRowParam> deliver = new List<BackDeliverRowParam>();
                foreach (var row in rowIds)
                {
                    deliver.Add(new BackDeliverRowParam { OrderNo = order, RowNo = row, DeliverNote = null, BackDeliverOnMaterialRow = false });
                }
                DeliverResult result = mPU.BackDeliver(deliver);
                return result.Succeeded.ToString();
            }
            catch
            {
                return "FAILED";
            }
        }

        //public IActionResult OrderRowMatrix(string onr)
        //{
        //    OrderMatrix omx = new OrderMatrix();
        //    ProductModel _pm = new ProductModel("", true);
        //    GIS_Dto.ProductModel pm = null;
        //    omx.ModelVariantList = new List<ModelVariant>();
        //    ProductVariant pv = null;
        //    int hvl = 0, mvl = 0;
        //    string model = "", variant = "";

        //    PurchaseOrderHead oh = mPU.GetOrderById(onr, true);

        //    if (oh != null)
        //    {
        //        foreach (OrderRow or in oh.OrderRows)
        //        {
        //            try
        //            {
        //                if (!or.ProductNo.Substring(hvl, mvl - hvl).Equals(variant) || hvl == 0)
        //                {
        //                    // If new model, get a new ProductModel
        //                    if (!or.ProductNo.StartsWith(model) || string.IsNullOrEmpty(model))
        //                    {
        //                        hvl = int.Parse(or.Product.HVL);
        //                        model = or.ProductNo.Substring(0, hvl);
        //                        pm = _pm.GetModel(or.ProductNo.Substring(0, hvl));
        //                        mvl = getIntValue(or.Product.MVL);
        //                    }

        //                    // Get the new variant code
        //                    variant = or.ProductNo.Substring(hvl, mvl - hvl);

        //                    // New modelDImension node
        //                    omx.ModelVariantList.Add(new ModelVariant());

        //                    // Add Model for info
        //                    omx.ModelVariantList.Last<ModelVariant>().Model = pm;

        //                    // Find Variant frpm model and add it for info
        //                    pv = pm.VariantList.Find(p => p.Code == variant);
        //                    omx.ModelVariantList.Last<ModelVariant>().Variant = pv;

        //                    // And finaly add a new sizelist for this ModelVariant node
        //                    omx.ModelVariantList.Last<ModelVariant>().SizeList = new List<Size>();
        //                }

        //                // Add orderrow so we can build our matrix with orderdata
        //                omx.ModelVariantList.Last<ModelVariant>().SizeList.Add(new Size());
        //                omx.ModelVariantList.Last<ModelVariant>().SizeList.Last<Size>().OrderRow = or;
        //                omx.ModelVariantList.Last<ModelVariant>().SizeList.Last<Size>().Dimension = pv.DimensionList.Find(d => d.Code == or.ProductNo.Substring(mvl));
        //            }
        //            catch (Exception e)
        //            {

        //            }

        //        }
        //    }


        //    return PartialView("OrderMatrixPartial", omx);
        //}

        public string BackDeliverOnOrderRow(string order, string row, string state, string delivernote)
        {
            try
            {
                List<BackDeliverRowParam> deliver = new List<BackDeliverRowParam>();
                BackDeliverRowParam dorp = new BackDeliverRowParam();
                OrderRow or = mPU.GetOrderRow(order, row);

                dorp.OrderNo = or.OrderNo;
                dorp.RowNo = or.RowNo;
                dorp.DeliverNote = string.IsNullOrEmpty(delivernote) ? "" : delivernote;

                deliver.Add(dorp);

                DeliverResult result = mPU.BackDeliver(deliver);
                return result.DeliverNo;
            }
            catch (Exception e)
            {
                return "FAILED";
            }
        }
        //public ActionResult PrintReceiptNote(string id)
        //{
        //    ERPReport report = new ERPReport();

        //    try
        //    {
        //        report.DocGen = (ConfigurationManager.GetSection("ReceiptNote") as NameValueCollection)["DocGen"].ToString();
        //        report.Report = (ConfigurationManager.GetSection("ReceiptNote") as NameValueCollection)["Report"].ToString();
        //        report.Medium = (ConfigurationManager.GetSection("ReceiptNote") as NameValueCollection)["Medium"].ToString();
        //        report.Form = (ConfigurationManager.GetSection("ReceiptNote") as NameValueCollection)["Form"].ToString();
        //        report.FilePath = (ConfigurationManager.GetSection("ReceiptNote") as NameValueCollection)["FilePath"].ToString();
        //        report.Wait = Convert.ToBoolean((ConfigurationManager.GetSection("ReceiptNote") as NameValueCollection)["Wait"].ToString());
        //    }
        //    catch
        //    {
        //        report.DocGen = "426";
        //        report.Report = "31";
        //        report.Medium = "P";
        //        report.Form = "A4SF";
        //    }

        //    report.IndexFrom = id;
        //    report.IndexTo = id;

        //    mPM.Print(report);

        //    return RedirectToAction("ViewDeliveredOrder/" + id);

        //}

        public IActionResult ViewDeliveredOrder(string id)
        {
            PurchaseOrderHead oh = mPU.GetOrderForDeliverNote(id);
            Supplier supplier = mSM.GetSupplierById(oh.SupplierNo);

            ViewBag.Supplier = supplier;

            if (oh.OrderRows == null)
                oh.OrderRows = new List<OrderRow>();

            return View(oh);
        }

        private int getIntValue(string value)
        {
            if (value == "A")
                return 10;
            else if (value == "B")
                return 11;
            else if (value == "C")
                return 12;
            else if (value == "D")
                return 13;
            else
                return int.Parse(value);
        }
    }
}