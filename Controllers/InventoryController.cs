﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BABC_Logistics_core.Database;
using BABC_Logistics_core.GIS.DTO;
using BABC_Logistics_core.GIS.Model;
using BABC_Logistics_core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProductModel = BABC_Logistics_core.GIS.DTO.ProductModel;

namespace BABC_Logistics_core.Controllers
{
    public class InventoryController : Controller
    {
        static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly InventoryContext _context;

        private GIS.Model.ProductModel mPM;

        public InventoryController(IConfiguration config, InventoryContext context)
        {
            mPM = new GIS.Model.ProductModel(config);

            _context = context;
        }

        // GET: Inventory
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult OverView()
        {
            InventoryVM result = new InventoryVM();

            try
            {
                result.ProductNo = "";
                result.Warehouse = "";
                result.InventoryWaitingForUpdate = new List<InventoryDetail>();

                //List<InventoryDetail> lst = Cache.DataCache.GetFromCache<InventoryDetail>("inventory");
                //if (lst == null)
                //    lst = new List<InventoryDetail>();

                //result.InventoryWaitingForUpdate = lst.OrderByDescending(o => o.Date).ThenByDescending(o => o.Time).ToList();

            }
            catch (Exception e)
            {

            }

            return View(result);

        }

        [HttpPost]
        public ActionResult OverView(InventoryVM param)
        {
            InventoryVM result = new InventoryVM();
            result.InventoryWaitingForUpdate = new List<InventoryDetail>();


            try
            {
                //List<InventoryDetail> lst = Cache.DataCache.GetFromCache<InventoryDetail>("inventory");
                //if (lst == null)
                //    lst = new List<InventoryDetail>();

                InventoryDetail newInventory = new InventoryDetail();
                var p = mPM.GetProductById(param.ProductNo);

                if (p?.ProductNo != null)
                {
                    var productCheck = CheckWarehouse(p, param);
                    newInventory.ProductNo = param.ProductNo;
                    newInventory.WarehouseNo = param.Warehouse;
                    newInventory.Amount = param.Amount;
                    newInventory.Date = DateTime.Now.ToString("yyMMdd");
                    newInventory.Time = DateTime.Now.ToString("HH:mm:ss");

                    if (productCheck.Item1 == true)
                    {
                        // Use Note to add at unique id to this row
                        newInventory.Note = Guid.NewGuid().ToString();

                        //lst.Add(newInventory);
                        //result.Warehouse = param.Warehouse;

                        //Cache.DataCache.AddToCache<List<InventoryDetail>>("inventory", 10, lst);
                    }
                    else
                    {
                        result.Message = productCheck.Item2;
                    }

                    //using (InventoryContext context = new InventoryContext())
                    //{
                    //    InventoryCache inventory = new InventoryCache();

                    //    inventory.ProductNo = newInventory.ProductNo;
                    //    inventory.Amount = newInventory.Amount;

                    //    context.Add(inventory);
                    //    context.SaveChanges();
                    //}

                }
                else
                {
                    result.Warehouse = param.Warehouse;
                }

                try
                {
                    //string path = Server.MapPath("~/inventory_backup_" + Path.GetRandomFileName() + ".txt");
                    //using (StreamWriter _testData = new StreamWriter(path, false))
                    //{
                    //    _testData.Write(JsonConvert.SerializeObject(lst)); // Write the file.
                    //}
                }
                catch (Exception e)
                {

                }

                // Add all products that waiting to be executeted
                //result.InventoryWaitingForUpdate = lst.OrderByDescending(o => o.Date).ThenByDescending(o => o.Time).ToList();

            }
            catch (Exception e)
            {

            }

            return View(result);


        }


        public string ReadBackup(string filename)
        {
            List<InventoryDetail> lst = new List<InventoryDetail>();
            string result = "";

            try
            {
                //using (StreamReader _testData = new StreamReader(Server.MapPath("~/" + filename), true))
                //{
                //    string json = _testData.ReadToEnd();

                //    lst = JsonConvert.DeserializeObject<List<InventoryDetail>>(json);

                //    Cache.DataCache.AddToCache<List<InventoryDetail>>("inventory", 10, lst);
                //}
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return result;
        }

        public string FindProduct(string search)
        {
            Product result = new Product();

            try
            {
                var p = mPM.GetProductById(search);

                if (p?.ProductNo != null)
                {
                    result = p;
                }
                else
                {
                    result = new Product { ProductNo = null, Description = "" };
                }
            }
            catch (Exception e)
            {

            }

            return JsonConvert.SerializeObject(result);
        }

        public ActionResult SumInventory()
        {
            List<InventorySumVM> result = new List<InventorySumVM>();

            try
            {
                //List<InventoryDetail> lst = Cache.DataCache.GetFromCache<InventoryDetail>("inventory");
                //var groupedList = lst.GroupBy(inventory => inventory.ProductNo);

                //foreach (var group in groupedList)
                //{
                //    InventorySumVM sum = new InventorySumVM();

                //    sum.Amount = group.Sum(s => s.Amount);
                //    sum.ProductNo = group.Key;
                //    sum.Warehouse = group.First().WarehouseNo;
                //    sum.InventoryDate = group.Last().Date;

                //    sum.ContainingInventories = group.ToList();

                //    result.Add(sum);
                //}
            }
            catch (Exception e)
            {

            }

            return View(result);
        }

        public ActionResult ExecuteInventory()
        {
            List<InventoryResult> result = new List<InventoryResult>();

            try
            {
                //List<InventoryParam> list = Cache.DataCache.GetFromCache<InventoryParam>("inventory");
                //var groupedList = list.GroupBy(inventory => inventory.ProductNo);

                //foreach (var sum in groupedList)
                //{
                //    InventoryParam inventory = new InventoryParam();

                //    inventory.ProductNo = sum.Key;
                //    inventory.Amount = sum.Sum(s => s.Amount);
                //    inventory.WarehouseNo = sum.First().WarehouseNo;
                //    inventory.Date = sum.First().Date;
                //    inventory.Time = sum.First().Time.Replace(":", "");

                //    InventoryResult invResult = mPM.ExecuteInventory(inventory);

                //    result.Add(invResult);

                //    if (invResult.Succeeded == true)
                //    {
                //        list.RemoveAll(r => r.ProductNo == sum.Key);

                //        Cache.DataCache.AddToCache<List<InventoryParam>>("inventory", 10, list);
                //    }
                //}
            }
            catch (Exception e)
            {
                logger.Error("Error in ExecuteInventory", e);
            }

            return View(result);
        }

        public string Delete(string id)
        {
            string result = "";

            try
            {
                //List<InventoryParam> lst = Cache.DataCache.GetFromCache<InventoryParam>("inventory");

                //var itemToRemove = lst.Where(i => i.Note == id).FirstOrDefault();

                //if (itemToRemove != null)
                //{
                //    lst.Remove(itemToRemove);
                //}

                //Cache.DataCache.AddToCache<List<InventoryParam>>("inventory", 10, lst);
            }
            catch (Exception e)
            {
                result = "false";
            }

            return result;
        }

        private Tuple<bool, string> CheckWarehouse(Product product, InventoryVM param)
        {
            Tuple<bool, string> result = new Tuple<bool, string>(false, "");

            //checked inventory on product if pressent
            if (product.WarehouseList?.Count > 0)
            {
                // Does the user send a warehouse?
                if (!string.IsNullOrEmpty(param.Warehouse))
                {
                    if (product.WarehouseList.Find(w => w.Id == param.Warehouse) != null)
                    {
                        result = new Tuple<bool, string>(true, "");
                    }
                    else
                    {
                        result = new Tuple<bool, string>(false, "Lagernr: " + param.Warehouse + " finns inte på artikel " + product.ProductNo);
                    }
                }
                else
                {
                    result = new Tuple<bool, string>(false, "Lagernr får ej vara blankt på artikel: " + product.ProductNo);
                }
            }
            else // Product is NOT a MultipleWarehouse product
            {
                if (!string.IsNullOrEmpty(param.Warehouse))
                {
                    result = new Tuple<bool, string>(false, "Lagernr: " + param.Warehouse + " skickades in men " + product.ProductNo + " är ingen flerlagerartikel");
                }
            }

            return result;
        }
    }
}