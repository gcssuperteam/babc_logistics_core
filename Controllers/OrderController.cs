﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BABC_Logistics_core.GIS.DTO;
using BABC_Logistics_core.GIS.Model;
using BABC_Logistics_core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NuGet.Frameworks;

namespace BABC_Logistics_core.Controllers
{
    public class OrderController : Controller
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private OrderModel mOM;
        private BaseDataModel mBDM;
        private LogisticModel mLOM;
        private UserModel mUM;
        private CustomerModel mCM;
        private PrintingModel mPM;
        private BABC_Logistics_core.GIS.Model.ProductModel mPRM;

        private string mGeneralIndexSearchCount = "0";

        public OrderController(IConfiguration config)
        {
            mOM = new OrderModel(config);
            mBDM = new BaseDataModel(config);
            mLOM = new LogisticModel(config);
            mUM = new UserModel(config);
            mCM = new CustomerModel(config);
            mPM = new PrintingModel(config);
            mPRM = new BABC_Logistics_core.GIS.Model.ProductModel(config);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(string search_value, string search_type)
        {
            List<OrderHead> lst = new List<OrderHead>();

            if (string.IsNullOrEmpty(search_value))
            {
                return View();
            }

            if (search_type.Equals("Datum"))
            {
                lst = mOM.GetOrderByIdxList("1", "*", "LDT>=" + search_value, false, 20, true);
            }
            else if (search_type.Equals("Kund"))
            {
                lst = mOM.GetOrderByIdxList("2", search_value, "LEF<=4", false, 50, false);
            }
            else if (search_type.Equals("Säljarkod"))
            {
                lst = mOM.GetOrderByIdxList("4", search_value, "*", false, 50, false);
            }

            List<OrderHead> sortedList = lst.OrderByDescending(o => o.OrderNo).ToList();

            ViewBag.search_value = search_value;
            ViewBag.search_type = search_type;

            return View(sortedList);
        }

        public IActionResult PickList(string search_value, string search_value2, string search_type)
        {
            List<OrderHead> lst = new List<OrderHead>();
            StringBuilder filter = new StringBuilder();
            List<OrderHead> tempLst = new List<OrderHead>();
            List<string> additionalLst = new List<string>();
            List<string> checkLst = new List<string>();

            PickListParam olp = new PickListParam();

            int maxOrdersInPicklist = int.MaxValue;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSetting["MaxOrdersInPicklist"]))
            {
                bool parseOK = int.TryParse(ConfigurationManager.AppSetting["MaxOrdersInPicklist"], out maxOrdersInPicklist);
            }

            // When landing on the page the first time
            if (string.IsNullOrEmpty(search_type))
                return View();

            if (search_type.Equals("supplie"))
            {
                try
                {
                    List<FilterDTO> filterList = getFilterList("FILTERS");
                    olp.Filter = parseFilterConstants(filterList[0].Filter);

                    if (filterList[0].UseCache == true)
                    {
                        olp.Cache = new CacheParam { CacheId = "supplie", ReadFrom = true, UpdateTo = false };
                    }

                }
                catch (Exception e)
                {
                    logger.Error("Error setting filter", e);
                }
            }
            else if (search_type.Equals("pre"))
            {
                try
                {
                    List<FilterDTO> filterList = getFilterList("FILTERO");
                    olp.Filter = parseFilterConstants(filterList[0].Filter);

                    if (filterList[0].UseCache == true)
                    {
                        olp.Cache = new CacheParam { CacheId = "pre", ReadFrom = true, UpdateTo = false };
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Error setting filter", e);
                }
            }
            else if (search_type.Equals("other"))
            {
                try
                {
                    List<FilterDTO> filterList = getFilterList("FILTERD");
                    olp.Filter = parseFilterConstants(filterList[0].Filter);

                    if (filterList[0].UseCache == true)
                    {
                        olp.Cache = new CacheParam { CacheId = "other", ReadFrom = true, UpdateTo = false };
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Error setting filter", e);
                }
            }

            olp.Index = 1;
            olp.IndexFilter = "*";
            olp.IndexCount = int.Parse(mGeneralIndexSearchCount);
            olp.OrderSerie = "A";
            olp.IncludeDeliveredRows = false;
            olp.WithRows = true;
            olp.MaxCount = 50;
            olp.ReverseReading = true;

            try
            {
                List<FilterDTO> filterList = getFilterList("FILTERA");
                if (filterList.Count > 0)
                {
                    olp.AdditionalFilter = parseFilterConstants(filterList[0].Filter);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error setting filter", e);
            }


            //// Additionlorders or ordinary search
            //if (search_type.Equals("customer"))
            //{
            //    olp.Index = 2;
            //    olp.IndexFilter = search_value;

            //    lst = mLOM.GetOrderList(olp);
            //}
            //else
            //{
            //    lst = mLOM.GetOrderListWithAdditionalOrder(olp);
            //}
            lst = mLOM.GetOrderListWithAdditionalOrder(olp);


            List<OrderHead> temp = new List<OrderHead>();
            if (lst != null)
            {
                foreach (OrderHead oh in lst)
                {
                    oh.PreferedDeliverDate = BABC_Logistics_core.Helpers.GeneralFunctions.getDateFromStr(oh.PreferedDeliverDate).ToString("yyyy-MM-dd");
                    oh.IsWholeOrderDeliverable = true;
                    oh.NonDeliverableRows = 0;
                    temp.Add(oh);

                    //foreach (OrderRow or in oh.OrderRows)
                    //{
                    //    if (or.Product != null)
                    //    {
                    //        if (or.Amount.Value <= or.Product.Stock)
                    //            oh.IsWholeOrderDeliverable = true;
                    //        else
                    //        {
                    //            oh.IsWholeOrderDeliverable = false;
                    //            oh.NonDeliverableRows++;
                    //        }
                    //    }
                    //}

                    //if (oh.OrderRows.Count > oh.NonDeliverableRows)
                    //{
                    //    temp.Add(oh);
                    //}

                }
                //temp = CheckTodayOrders(lst);
            }

            //ViewBag.additionalOrders = additionalLst;
            ViewBag.search_value = search_value;
            ViewBag.search_type = search_type;

            List<OrderHead> sortedList = temp.OrderBy(o => o.PickListState)
                .ThenBy(o => o.PreferedDeliverDate)
                .ThenBy(o => o.Date)
                .ThenBy(o => o.Order_ttmm)
                .Take(maxOrdersInPicklist)
                .ToList();

            return View(sortedList);
        }

        public IActionResult Details(string id)
        {
            List<OrderHead> lst = mOM.GetOrderPickList("1", id, "0", "*", "A", true, false, 1, false);
            OrderHead oh = null;
            Dictionary<string, List<MaterialRow>> materials = new Dictionary<string, List<MaterialRow>>();

            if (lst.Count > 0)
                oh = lst[0];
            else
                oh = null;

            if (oh != null)
            {
                BaseTable ls = mBDM.GetBaseTable("03", oh.DeliverWay);
                if (ls != null)
                    oh.DeliverWay = ls.Description;

                BaseTable lv = mBDM.GetBaseTable("02", oh.DeliverTerms);
                if (lv != null)
                    oh.DeliverTermsDescription = lv.Description;

                //Customer customer = mCM.GetCustomerById(oh.DeliverCustomerNo);

                //ViewBag.Customer = customer;

            }

            if (oh.OrderRows == null)
                oh.OrderRows = new List<OrderRow>();

            // Remove all rows without product
            oh.OrderRows.RemoveAll(o => string.IsNullOrEmpty(o.ProductNo));

            logger.Debug("Details materials :" + materials.Count);
            ViewBag.Materials = materials;

            var sortedRows = (from OrderRow r in oh.OrderRows
                              orderby r.Product.WarehouseNo ascending
                              select r).ToList();

            oh.OrderRows = sortedRows;
            oh.isPrioOrder = false;

            return View(oh);
        }


        public IActionResult PickOrder(string id, string sign)
        {
            List<OrderHead> lst = mOM.GetOrderPickList("1", id, "0", "*", "A", true, true, 1, false);
            OrderViewModel ovm = new OrderViewModel();
            ovm.Order = new OrderHead();

            // Disabla check on "under picking"
            //sign = "rockon";

            if (lst.Count > 0)
            {
                ovm.Order = lst[0];

                //if (!ovm.Order.PickListState.Equals("5") || (sign.Equals("rockon")))
                // {
                //ovm.Order.PickListState = "5";
                ovm.Order.PickListState = "P";
                mOM.UpdateOrderHead(ovm.Order);

                BaseTable ls = mBDM.GetBaseTable("03", ovm.Order.DeliverWay);
                if (ls != null)
                    ovm.Order.DeliverWay = ls.Description;

                BaseTable lv = mBDM.GetBaseTable("02", ovm.Order.DeliverTerms);
                if (lv != null)
                    ovm.Order.DeliverTermsDescription = lv.Description;

                // Remove all rows without product
                ovm.Order.OrderRows.RemoveAll(o => string.IsNullOrEmpty(o.ProductNo));

                List<OrderRow> tempList = new List<OrderRow>();
                foreach (OrderRow row in ovm.Order.OrderRows)
                {
                    if(!string.IsNullOrEmpty(row.ProductNo))
                    {
                        if (row.Product.StockUpdateType.CompareTo("6") < 0 || row.ProductNo.Equals("RAD"))
                        {
                            tempList.Add(row);
                        }
                    }
                }

                ovm.Order.OrderRows = tempList;

                try
                {
                    var sortedRows = (from OrderRow r in ovm.Order.OrderRows
                                      orderby r.Product.WarehouseNo ascending
                                      select r).ToList();

                    ovm.Order.OrderRows = sortedRows;
                }
                catch { }
                //hämta kundfält för PS info
                Customer customer = mCM.GetCustomerById(ovm.Order.DeliverCustomerNo);

                ViewBag.Customer = customer;
                //oh.isPrioOrder = CheckPrioOrder(oh.OrderNo);

                List<FilterDTO> filterList = getFilterList("FILTERE");

                if (filterList?.Count > 0)
                {
                    ovm.SelectableProducts = mPRM.GetProductList("1", "*", filterList[0].Filter);
                }
                else
                {
                    ovm.SelectableProducts = new List<Product>();
                }

                //}
                //else
                //{
                //    return RedirectToAction("OrderIsAlreadyUnderPicking", "Order", new { order = ovm.Order.OrderNo, sign = "rockon", forwardpath = "/Order/PickOrder/" + ovm.Order.OrderNo + "/rockon", backpath = "/Order/PickList" });
                //}
            }
            else
                ovm.Order = null;



            return View(ovm);
        }

        public IActionResult CancelPickOrder(string id)
        {
            List<OrderHead> lst = mOM.GetOrderPickList("1", id, "0", "*", "A", true, false, 1, false);
            OrderHead oh = null;

            if (lst.Count > 0)
            {
                oh = lst[0];
                oh.PickListState = "5";
                mOM.UpdateOrderHead(oh);
            }
            else
                oh = null;

            return RedirectToAction("Details", "Order", new { id });
        }


        //[HttpPost]
        //public ActionResult DeliverOrder(List<OrderRow> rows)
        //{
        //    List<DeliverRowParam> deliver = new List<DeliverRowParam>();
        //    //OrderRow or = mOM.GetOrderRow(order, row);
        //    string decSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        //    decimal? dAmount = null;

        //    try
        //    {
        //        if (decSep == ".")
        //        {
        //            dAmount = decimal.Parse(amount.Replace(",", "."));
        //        }
        //        else
        //        {
        //            dAmount = decimal.Parse(amount.Replace(".", ","));
        //        }
        //    }
        //    catch { }

        //    try
        //    {
        //        if (dAmount.HasValue)
        //        {
        //            if (dAmount.Value != 0)
        //            {
        //                //or.DeliverState = state;
        //                //if (dAmount.HasValue)
        //                //    or.AmountToDeliver = dAmount.Value;

        //                DeliverRowParam param = new DeliverRowParam()
        //                {
        //                    AmountToDeliver = dAmount.Value,
        //                    DeliverDate = "",
        //                    DeliverMaterialRow = false,
        //                    DeliverNote = "",
        //                    DeliverState = state,
        //                    HandleOperationRow = false,
        //                    OrderNo = order,
        //                    RowNo = int.Parse(row),
        //                    WarehouseNo = ""
        //                };

        //                deliver.Add(param);
        //            }
        //            else
        //            {
        //                if (state == "5")
        //                {
        //                    DeliverRowParam param = new DeliverRowParam()
        //                    {
        //                        AmountToDeliver = dAmount.Value,
        //                        DeliverDate = "",
        //                        DeliverMaterialRow = false,
        //                        DeliverNote = "",
        //                        DeliverState = state,
        //                        HandleOperationRow = false,
        //                        OrderNo = order,
        //                        RowNo = int.Parse(row),
        //                        WarehouseNo = ""
        //                    };

        //                    deliver.Add(param);
        //                }
        //                else
        //                {
        //                    return "FAILED";
        //                }
        //            }

        //            DeliverResult result = mOM.Deliver(deliver);

        //            if (result.Succeeded)
        //            {
        //                return result.DeliverNo;
        //            }
        //            else
        //            {
        //                return "FAILED - " + result.InternalDeliverMessage;
        //            }
        //        }
        //        else
        //        {
        //            return "FAILED";
        //        }
        //    }
        //    catch
        //    {
        //        return "FAILED";
        //    }
        //}


        public IActionResult ViewDeliveredOrder(string id)
        {
            OrderHead oh = mOM.GetOrderForDeliverNote(id);
            Customer customer = mCM.GetCustomerById(oh.DeliverCustomerNo);

            ViewBag.Customer = customer;

            if (oh.OrderRows == null)
                oh.OrderRows = new List<OrderRow>();

            return View(oh);
        }

        public ActionResult PrintDeliverNote(string id)
        {
            ERPReport report = new ERPReport();

            try
            {
                report.DocGen = ConfigurationManager.AppSetting["DeliverNote:DocGen"];
                report.Report = ConfigurationManager.AppSetting["DeliverNote:Report"];
                report.Medium = ConfigurationManager.AppSetting["DeliverNote:Medium"];
                report.Form = ConfigurationManager.AppSetting["DeliverNote:Form"];
                report.FilePath = ConfigurationManager.AppSetting["DeliverNote:FilePath"];
                report.Wait = Convert.ToBoolean(ConfigurationManager.AppSetting["DeliverNote:Wait"]);
            }
            catch
            {
                report.DocGen = "296";
                report.Report = "M1";
                report.Medium = "P";
                report.Form = "FS";
            }

            report.IndexFrom = id;
            report.IndexTo = id;

            if (mPM.Print(report))
            {
                //Uppdatera PLF på orderhuvud till 5, samt alla rader där LEF!=5 till PLF=5
                try
                {
                    OrderHead oh = mOM.GetOrderForDeliverNote(id);
                    List<OrderRow> orLst = mOM.GetOrderRowByOrderIdList(oh.OrderNo);

                    oh.PickListState = "5";
                    orLst.ForEach(x => { if (x.DeliverState != "5") { x.PickListState = "5"; } });

                    mOM.UpdateOrderHead(oh);
                    mOM.UpdateOrderRowList(orLst);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return RedirectToAction("ViewDeliveredOrder", "Order", new { id });
        }

        [HttpPost]
        public string DeliverOrderRow(string order, string row, string state, string amount)
        {
            List<DeliverRowParam> deliver = new List<DeliverRowParam>();
            //OrderRow or = mOM.GetOrderRow(order, row);
            string decSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            decimal? dAmount = null;

            try
            {
                if (decSep == ".")
                {
                    dAmount = decimal.Parse(amount.Replace(",", "."));
                }
                else
                {
                    dAmount = decimal.Parse(amount.Replace(".", ","));
                }
            }
            catch { }

            try
            {
                if (dAmount.HasValue)
                {
                    if (dAmount.Value != 0)
                    {
                        //or.DeliverState = state;
                        //if (dAmount.HasValue)
                        //    or.AmountToDeliver = dAmount.Value;

                        DeliverRowParam param = new DeliverRowParam()
                        {
                            AmountToDeliver = dAmount.Value,
                            DeliverDate = "",
                            DeliverMaterialRow = false,
                            DeliverNote = "",
                            DeliverState = state,
                            HandleOperationRow = false,
                            OrderNo = order,
                            RowNo = int.Parse(row),
                            WarehouseNo = ""
                        };

                        deliver.Add(param);
                    }
                    else
                    {
                        if (state == "5")
                        {
                            DeliverRowParam param = new DeliverRowParam()
                            {
                                AmountToDeliver = dAmount.Value,
                                DeliverDate = "",
                                DeliverMaterialRow = false,
                                DeliverNote = "",
                                DeliverState = state,
                                HandleOperationRow = false,
                                OrderNo = order,
                                RowNo = int.Parse(row),
                                WarehouseNo = ""
                            };

                            deliver.Add(param);
                        }
                        else
                        {
                            return "FAILED";
                        }
                    }

                    DeliverResult result = mOM.Deliver(deliver);

                    if (result.Succeeded)
                    {
                        setReservationCode(order, row);

                        return result.DeliverNo;
                    }
                    else
                    {
                        return "FAILED - " + result.InternalDeliverMessage;
                    }
                }
                else
                {
                    return "FAILED";
                }
            }
            catch
            {
                return "FAILED";
            }

        }


        public string DeliverOrderRows(string order, List<DeliverRowParam> rows, string state)
        {
            string decSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            try
            {
                foreach (var row in rows)
                {
                    if (decSep == ".")
                    {
                        row.AmountToDeliver = decimal.Parse(row.AmountToDeliver.ToString().Replace(",", "."));
                    }
                    else
                    {
                        row.AmountToDeliver = decimal.Parse(row.AmountToDeliver.ToString().Replace(".", ","));
                    }

                    row.DeliverDate = "";
                    row.DeliverMaterialRow = false;
                    row.DeliverNote = "";
                    row.DeliverState = state;
                    row.HandleOperationRow = false;
                    row.OrderNo = order;
                    row.WarehouseNo = "";
                }

            }
            catch { }

            try
            {
                DeliverResult result = mOM.Deliver(rows);
                return result.DeliverNo;

            }
            catch
            {
                return "FAILED";
            }


        }


        public string BackDeliverOnOrderRow(string order, string row, string state)
        {
            List<BackDeliverRowParam> deliver = new List<BackDeliverRowParam>();
            //OrderRow or = mOM.GetOrderRow(order, row);

            try
            {
                deliver.Add(new BackDeliverRowParam { BackDeliverOnMaterialRow = false, DeliverNote = null, OrderNo = order, RowNo = int.Parse(row) });

                DeliverResult result = mOM.BackDeliver(deliver);

                setReservationCode(order, row);

                return result.Succeeded.ToString();
            }
            catch
            {
                return "FAILED";
            }
        }

        public string BackDeliverOnOrderRows(string order, int[] rowIds)
        {
            try
            {
                List<BackDeliverRowParam> deliver = new List<BackDeliverRowParam>();
                foreach (var row in rowIds)
                {
                    deliver.Add(new BackDeliverRowParam { OrderNo = order, RowNo = row, DeliverNote = null, BackDeliverOnMaterialRow = false });
                }
                DeliverResult result = mOM.BackDeliver(deliver);
                return result.Succeeded.ToString();
            }
            catch
            {
                return "FAILED";
            }
        }

        private string parseFilterConstants(string filter)
        {
            int count = 0;

            if (filter.Contains("#DAT"))
            {
                while (filter.Contains("#DAT"))
                {

                    int pos = filter.IndexOf("#DAT");
                    string whole = filter.Substring(pos, 6);
                    string date = DateTime.Now.AddDays(int.Parse(whole.Substring(4))).ToString("yyMMdd");
                    filter = filter.Replace(whole, date);
                    count++;

                    // If this high count something went wrong
                    if (count > 20)
                    {
                        break;
                    }
                }
            }

            return filter;
        }

        private List<FilterDTO> getFilterList(string prefix)
        {
            List<FilterDTO> result = new List<FilterDTO>();

            try
            {
                List<BaseTable> filterlist = mBDM.GetBaseTableList(prefix);

                if (filterlist != null)
                {
                    foreach (BaseTable bt in filterlist)
                    {
                        result.Add(new FilterDTO { Id = bt.Id, Description = bt.Description, Type = bt.Type, Filter = bt.Description2, UseCache = bt.Code1 == "T" ? true : false });
                    }
                }
            }
            catch (Exception)
            {

            }

            return result;
        }

        public string ValidateSignature(string sign)
        {
            Console.WriteLine("ValidateSignature: " + sign);

            if (!string.IsNullOrEmpty(sign))
            {
                User user = mUM.GetUser("", sign.ToUpper());

                if (user.UserName != null)
                    return "true";
                else
                    return "false";
            }
            else
                return "FAILED";

        }

        private int getIntValue(string value)
        {
            if (value == "A")
                return 10;
            else if (value == "B")
                return 11;
            else if (value == "C")
                return 12;
            else if (value == "D")
                return 13;
            else
                return int.Parse(value);
        }

        private void setReservationCode(string order, string row)
        {
            try
            {
                OrderRow orderrow = mOM.GetOrderRow(order, row);

                if (orderrow.DeliverState != "5")
                {
                    orderrow.ReservedId = "";

                    mOM.UpdateOrderRowList(new List<OrderRow> { orderrow });
                }
            }
            catch(Exception e)
            {

            }

        }
    }

    static class ConfigurationManager
    {
        public static IConfiguration AppSetting { get; }
        static ConfigurationManager()
        {
            AppSetting = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
        }
    }
}