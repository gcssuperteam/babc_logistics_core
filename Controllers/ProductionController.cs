﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BABC_Logistics_core.GIS.DTO;
using BABC_Logistics_core.GIS.Model;
using BABC_Logistics_core.Helpers;
using BABC_Logistics_core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BABC_Logistics_core.Controllers
{
    public class ProductionController : Controller
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ProductionModel mPM;
        private BABC_Logistics_core.GIS.Model.ProductModel mProduct;
        private BABC_Logistics_core.GIS.Model.OrderModel mOM;

        public ProductionController(IConfiguration config)
        {
            mPM = new ProductionModel(config);
            mProduct = new BABC_Logistics_core.GIS.Model.ProductModel(config);
            mOM = new BABC_Logistics_core.GIS.Model.OrderModel(config);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult OperationList()
        {
            return View();
        }

        public IActionResult Location(string onr, string row, string matrow, string opn)
        {
            WarehouseMoveVM result = new WarehouseMoveVM();

            try
            {
                ProductionOrderHead order = mPM.GetMaterialOrder(onr, row);
                MaterialRow materialRow = order.MaterialRows.Where(r => r.MaterialRowNo == int.Parse(matrow)).First();

                Product product = mProduct.GetProductById(materialRow.ProductNo);

                result.OrderNo = onr;
                result.RowNo = row;
                result.MaterialRow = matrow;
                result.OperationNo = opn;
                result.ProductNo = product.ProductNo;
                result.WarehouseNoTo = materialRow.WarehouseNo;
                
                result.WarehouseList = product.WarehouseList;
                result.WarehouseList.RemoveAll(w => w.Stock <= 0);
                result.Amount = GeneralFunctions.getStrFromDecimal(materialRow.Amount);

                

                WarehouseNumber wn = product.WarehouseList.Where(w => w.Id == materialRow.WarehouseNo).First();

                if (wn != null)
                {
                    if (materialRow.Amount > wn.Stock)
                    {
                        decimal currentStock = 0;

                        // If stock is less then 0, we set i to zero
                        if (wn.Stock >= 0)
                        {
                            currentStock = wn.Stock;
                        }

                        result.Amount = (materialRow.Amount - currentStock).ToString();
                    }
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in Location()", e);
            }

            return View(result);
        }


        [HttpPost]
        public IActionResult ExecuteWarehouseMove(WarehouseMoveVM model)
        {
            try
            {
                MoveParam mp = new MoveParam();
                mp.ProductNo = model.ProductNo;
                mp.MoveFrom = model.WarehouseNoFrom;
                mp.MoveTo = model.WarehouseNoTo;
                mp.Amount = GeneralFunctions.getDecimalFromStr(model.Amount);

                MoveResult result = mProduct.MoveProductBetweenWarehouses(mp);

                //MaterialPickVM pickResult = pickMaterial(model.ProductNo, model.OrderNo, model.RowNo, model.OperationNo);

                if (!result.Succeeded)
                {
                    mLog.Debug(result.InternalMoveMessage);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in ExecuteWarehouseMove()", e);
            }

            return RedirectToAction("MaterialList", "Production", new { onr = model.OrderNo, row = model.RowNo, opn = model.OperationNo });
        }

        [HttpPost]
        public IActionResult OperationList(string order_and_row)
        {
            ProductionOrderHead result = new ProductionOrderHead();

            try
            {
                string[] sOrderAndRow = order_and_row.Split(new char[] { ' ', '-' });

                if (sOrderAndRow.Length > 1)
                {
                    result = mPM.GetProductionOrder(sOrderAndRow[0].Trim(), sOrderAndRow[sOrderAndRow.Length-1].Trim());

                    // Remove operations that are already done
                    result.OperationRows = result.OperationRows.Where(o => o.Status != "5")?.ToList();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error getting Operations", e);
            }

            return View(result);
        }

        public IActionResult MaterialList(string onr, string row, string opn)
        {
            MaterialPickVM result = new MaterialPickVM();

            try
            {
                result.OH = mPM.GetMaterialOrder(onr, row);
                List<MaterialRow> tempList = new List<MaterialRow>();

                // FIlter out only materialrows connected to selected operation
                foreach (var matrow in result.OH.MaterialRows)
                {
                    if (matrow.OperationNo == opn)
                    {
                        matrow.Product.WarehouseList.RemoveAll(w=>w.Stock <= 0);
                        tempList.Add(matrow);
                    }
                    else
                    {
                        mLog.Debug("Could not find operationno on materialrow: " + opn);
                    }
                }
                
                result.OH.MaterialRows = tempList;
                result.OperationNo = opn;
                result.Succeeded = true;
            }
            catch (Exception e)
            {
                mLog.Error("Error in MaterialList", e);
            }

            return View(result);
        }

        public IActionResult PickMaterial(string productno, string onr, string row, string opn)
        {
            MaterialPickVM result = new MaterialPickVM();

            try
            {
                result = pickMaterial(productno, onr, row, opn);

                Product product = mProduct.GetProductById(productno);

                // Extra_VG/Varugrupp under Säsong i Garp
                if(product != null)
                {
                    if (product.ProductGroup5 == "EH")
                    {
                        ProductLabel pl = new ProductLabel();

                        OrderRow or = mOM.GetOrderRow(onr, row);

                        pl.LabelProductNo = productno;
                        pl.ProductNoBeingManufactured = or.ProductNo;

                        result.Label = pl;
                    }
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in PickMaterial", e);

            }

            return PartialView("_MaterialRows", result);
        }

        private MaterialPickVM pickMaterial(string productno, string onr, string row, string opn )
        {
            MaterialPickVM result = new MaterialPickVM();

            try
            {
                List<MaterialRow> tempList = new List<MaterialRow>();
                result.OH = mPM.GetMaterialOrder(onr, row);

                // FIlter out only materialrows connected to selected operation
                foreach (var matrow in result.OH.MaterialRows)
                {
                    if (matrow.OperationNo == opn)
                    {
                       
                        tempList.Add(matrow);
                    }
                }

                // Check if we find product on this result
                var found = tempList.Find(m => m.ProductNo == productno);
                if (found != null)
                {
                    found.ReservationCode = "5";
                    mPM.UpdateMaterialRow(found);

                    result.Succeeded = true;
                    result.MessageHeader = "Pick done!";
                    result.Message = "Picked " + found.Amount + " of product " + found.ProductNo + " - " + found.ProductDescription + " from warehouseno: " + found.WarehouseNo;
                    result.ShowMessage = true;
                }
                else
                {
                    result.Succeeded = false;
                    result.MessageHeader = "Pick failed";
                    result.Message = "Could not find product " + productno + " in material list!";
                    result.ShowMessage = true;
                }

                result.OH.MaterialRows = tempList;
            }
            catch(Exception e)
            {
                mLog.Error("Error in pickMaterial", e);
            }

            return result;
        }

        [HttpPost]
        public IActionResult BackMaterial(string onr, string row, string matrow, string opn)
        {
            MaterialPickVM result = new MaterialPickVM();

            try
            {
                result.OH = mPM.GetMaterialOrder(onr, row);
                List<MaterialRow> tempList = new List<MaterialRow>();

                // Filter out only materialrows connected to selected operation
                foreach (var mr in result.OH.MaterialRows)
                {
                    if (mr.OperationNo == opn)
                    {
                        tempList.Add(mr);
                    }
                }

                result.OH.MaterialRows = tempList;

                // Check if we find product on this result
                var found = result.OH.MaterialRows.Find(m => m.MaterialRowNo == int.Parse(matrow));
                if (found != null)
                {
                    found.ReservationCode = "";
                    mPM.UpdateMaterialRow(found);

                    result.Succeeded = true;
                    result.MessageHeader = "Back done!";
                    result.Message = "Backed " + found.Amount + " of product " + found.ProductNo + " - " + found.ProductDescription + " from warehouseno: " + found.WarehouseNo;
                    result.ShowMessage = true;
                }
                else
                {
                    result.Succeeded = false;
                    result.MessageHeader = "Back failed";
                    result.Message = "Could not find materialrow " + matrow + " on order " + onr + "-" + row;
                    result.ShowMessage = true;
                }

                result.OH.MaterialRows = tempList;
            }
            catch (Exception e)
            {
                mLog.Error("Error in PickMaterial", e);

            }

            return PartialView("_MaterialRows", result);



            //MaterialPickVM result = new MaterialPickVM();
            //string opn = "";

            //try
            //{
            //    result.OH = mPM.GetMaterialOrder(onr, row);

            //    // Check if we find product on this result
            //    var found = result.OH.MaterialRows.Find(m => m.MaterialRowNo == int.Parse(matrow));
            //    if (found != null)
            //    {
            //        found.ReservationCode = "";
            //        mPM.UpdateMaterialRow(found);
            //        result.Succeeded = true;

            //        opn = found.OperationNo;
            //    }
            //    else
            //    {
            //        result.Succeeded = false;
            //    }
            //}
            //catch (Exception e)
            //{
            //    mLog.Error("Error in BackMaterial", e);
            //}

            //return PartialView("_MaterialRows", result);
            ////return RedirectToAction("MaterialList", "Production", new { onr = onr, row = row, opn = opn });
        }
    }
}