﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.Database
{
    public class InventoryContext : DbContext
    {
        public InventoryContext(DbContextOptions<InventoryContext> options) : base(options)
        {

        }

        public DbSet<InventoryCache> Inventory { get; set; }
    }

    public class InventoryCache
    {
        public string ID { get; set; }
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
        public decimal OriginalAmount { get; set; }
        public DateTime AmountAtTime { get; set; }
        public decimal VIPAtTime { get; set; }
        public bool InventoryDone { get; set; }
    }
}
