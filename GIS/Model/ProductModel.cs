﻿using BABC_Logistics_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.Model
{
    public class ProductModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ProductModel(IConfiguration config) : base(config)
        {

        }

        public List<Product> GetProductList(string index, string indexfilter, string general_filter_string)
        {
            string rest = "";
            List<Product> lst = new List<Product>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/ProductSvc/REST/GetProductList" + GISToken + "/" + index + "/" + indexfilter + "/" + general_filter_string;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Timeout = 600000;

                WebResponse ws = request.GetResponse();

                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<Product>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        //public List<ProductModel> GetModelList(string index, string indexfilter, string general_filter_string)
        //{
        //    string rest = "";
        //    List<GIS_Dto.ProductModel> lst = new List<GIS_Dto.ProductModel>();

        //    try
        //    {
        //        rest = getAddress("/REST/GetModelList") + "/" + index + "/" + indexfilter + "/" + general_filter_string;

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);
        //        request.Timeout = 1200000;

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        lst = JsonConvert.DeserializeObject<List<GIS_Dto.ProductModel>>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return lst;
        //}

        //public List<WarehouseNumber> GetProductWarehouseList(string productno)
        //{
        //    string rest = "";
        //    List<WarehouseNumber> lst = new List<WarehouseNumber>();

        //    try
        //    {
        //        rest = getAddress("/REST/GetProductWarehouseList") + "/" + productno;
        //        rest = rest.Replace("ProductSvc", "WarehouseSvc");
        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);

        //        WebResponse ws = request.GetResponse();
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        lst = JsonConvert.DeserializeObject<List<WarehouseNumber>>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return lst;
        //}


        public MoveResult MoveProductBetweenWarehouses(MoveParam param)
        {
            string rest = "";
            MoveResult result = new MoveResult();

            try
            {

                rest = GISBaseAddress.TrimEnd('/') + "/WarehouseSvc/REST/MoveProductLocation/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                try
                {
                    result = JsonConvert.DeserializeObject<MoveResult>(response);
                }
                catch (Exception e)
                {
                    mLog.Error("Error parsing json in MoveProductBetweenWarehouses ", e);
                    result.Succeeded = true;
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                result.Succeeded = false;
                result.InternalMoveMessage = e.Message;
            }
            return result;
        }

        //public GIS_Dto.ProductModel GetModel(string productno)
        //{
        //    string rest = "";
        //    GIS_Dto.ProductModel product = new GIS_Dto.ProductModel();

        //    rest = getAddress("/REST/GetModel") + "/" + productno;

        //    mLog.Debug("CALL TO: " + rest);

        //    try
        //    {
        //        WebRequest request = WebRequest.Create(rest);

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        product = JsonConvert.DeserializeObject<GIS_Dto.ProductModel>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return product;
        //}
        //public string GetModelFromId(string productno)
        //{
        //    string rest = "";

        //    string modelNo = "";
        //    rest = getAddress("/REST/GetModelFromId") + "/" + productno;

        //    mLog.Debug("CALL TO: " + rest);

        //    try
        //    {
        //        WebRequest request = WebRequest.Create(rest);

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        modelNo = JsonConvert.DeserializeObject<string>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return modelNo;
        //}

        public Product GetProductById(string id)
        {
            string rest = "";
            Product product = new Product();

            rest = GISBaseAddress.TrimEnd('/') + "/ProductSvc/REST/GetProduct/" + GISToken + "/" + id;

            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                product = JsonConvert.DeserializeObject<Product>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return product;
        }

        //public ProductCalculation GetProductCalculationById(string id)
        //{
        //    string rest = "";
        //    ProductCalculation pc = new ProductCalculation();

        //    rest = getAddress("/REST/GetProductCalculation") + "/" + id;

        //    mLog.Debug("CALL TO: " + rest);

        //    try
        //    {
        //        WebRequest request = WebRequest.Create(rest);

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        WebResponse ws = request.GetResponse();
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        pc = JsonConvert.DeserializeObject<ProductCalculation>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return pc;
        //}

        //public bool AddProductCalculation(ProductCalculation pc)
        //{
        //    string rest = "";

        //    try
        //    {
        //        rest = getAddress("/REST/AddProductCalculation");

        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(pc));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            string result = reader.ReadToEnd();
        //            if (result.Equals("true"))
        //                return true;
        //            else
        //                return false;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //        return false;
        //    }
        //}

        //public Product ProductSearch(ProductParam param)
        //{
        //    string rest = "";
        //    Product product = new Product();

        //    try
        //    {

        //        rest = getAddress("/REST/ProductSearch");

        //        //mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(param));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            product = JsonConvert.DeserializeObject<Product>(reader.ReadToEnd());
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        //mLog.Error("Error in call: " + rest, e);
        //    }

        //    return product;
        //}

        //public bool AddProduct(Product product)
        //{
        //    string rest = "";

        //    try
        //    {

        //        rest = getAddress("/REST/AddProduct");

        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";

        //        //if (!string.IsNullOrEmpty(Token))
        //        //{
        //        //    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //        //    request.Headers.Add("Authorization", "Basic " + encoded);
        //        //}

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(product));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            string result = reader.ReadToEnd();
        //            if (result.Equals("true"))
        //                return true;
        //            else
        //                return false;
        //            // do something with the results
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //        return false;
        //    }
        //}

        //public List<ProductModelResult> UpdateProductModels(List<GIS_Dto.ProductModel> list)
        //{
        //    string rest = "";
        //    List<ProductModelResult> result = new List<ProductModelResult>();

        //    try
        //    {

        //        rest = getAddress("/REST/UpdateModels");

        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(list));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            result = JsonConvert.DeserializeObject<List<ProductModelResult>>(reader.ReadToEnd());
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return result;

        //}

        //public ProductUpdateResponse UpdateProduct(Product product)
        //{
        //    ProductUpdateResponse p = new ProductUpdateResponse();
        //    string rest = "";
        //    try
        //    {

        //        rest = getAddress("/REST/UpdateProduct");

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(product));
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        p = JsonConvert.DeserializeObject<ProductUpdateResponse>(response);

        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //        p.Succeeded = false;
        //        p.InternalDeliverMessage = e.Message;
        //    }
        //    return p;
        //}

        //public List<Product> GetProductListFromIdList(List<string> productNos)
        //{
        //    List<Product> p = new List<Product>();
        //    string rest = "";
        //    try
        //    {

        //        rest = getAddress("/REST/GetProductListFromIdList");

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(productNos));
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        p = JsonConvert.DeserializeObject<List<Product>>(response);

        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }
        //    return p;
        //}

        //public bool DeleteProduct(Product product)
        //{
        //    string rest = "";

        //    rest = getAddress("/REST/DeleteProduct");

        //    mLog.Debug("CALL TO: " + rest);

        //    try
        //    {
        //        WebRequest request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(product));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            string result = reader.ReadToEnd();
        //            if (result.Equals("true"))
        //                return true;
        //            else
        //                return false;
        //            // do something with the results
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //        return false;
        //    }
        //}

        //public UpdateResult UpdateStockLocForModel(UpdateTableRangeObject o)
        //{
        //    UpdateResult r = new UpdateResult();
        //    string rest = "";
        //    try
        //    {

        //        rest = getGenericSvcAddress("/REST/UpdateTableByRange");

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(o));
        //        }

        //        WebResponse ws = request.GetResponse();
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        r = JsonConvert.DeserializeObject<UpdateResult>(response);

        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //        r.Succeeded = false;
        //        r.InternalMoveMessage = e.Message;
        //    }

        //    return r;
        //}

        //public ProductStockResult GetProductAvailability(ProductAvailabilityParam param)
        //{
        //    ProductStockResult p = new ProductStockResult();
        //    string rest = "";
        //    try
        //    {

        //        rest = getAddress("/REST/GetProductAvailability");

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(param));
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        p = JsonConvert.DeserializeObject<ProductStockResult>(response);

        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //        p.Error = true;
        //        p.Message = e.Message;
        //    }
        //    return p;
        //}

        //public WebText GetWebText(GetWebTextParam param)
        //{
        //    WebText wb = new WebText();
        //    string rest = "";

        //    try
        //    {

        //        rest = getAddress("/REST/GetWebText");

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(param));
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        wb = JsonConvert.DeserializeObject<WebText>(response);

        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return wb;
        //}

        //public decimal GetStockAtDate(string productno, string warehouse, string at_date, string calculate_method)
        //{
        //    string rest = "";
        //    decimal stock = 0;

        //    if (string.IsNullOrEmpty(warehouse))
        //        warehouse = "*";

        //    rest = getAddress("/REST/GetStockAtDate") + "/" + productno + "/" + warehouse + "/" + at_date + "/" + calculate_method;

        //    mLog.Debug("CALL TO: " + rest);

        //    try
        //    {
        //        WebRequest request = WebRequest.Create(rest);

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        stock = JsonConvert.DeserializeObject<decimal>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return stock;
        //}


        //public List<Product> FillProductListWithStockAtDate(List<Product> product, string warehouse, string at_date, string calculate_method)
        //{
        //    string rest = "";
        //    List<Product> lst = new List<Product>();

        //    rest = getAddress("/REST/FillProductListWithStockAtDate") + "/" + warehouse + "/" + at_date + "/" + calculate_method;

        //    mLog.Debug("CALL TO: " + rest);

        //    try
        //    {
        //        WebRequest request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(product));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            lst = JsonConvert.DeserializeObject<List<Product>>(reader.ReadToEnd());
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return lst;
        //}

        //public List<ProductModelResult> AddProductModel(List<GIS_Dto.ProductModel> models)
        //{
        //    string rest = "";
        //    List<ProductModelResult> result = new List<ProductModelResult>();

        //    try
        //    {

        //        rest = getAddress("/REST/AddModels");

        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(models));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            result = JsonConvert.DeserializeObject<List<ProductModelResult>>(reader.ReadToEnd());
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return result;
        //}

        //public List<ProductVariantResult> AddVariants(List<GIS_Dto.ProductVariant> variants)
        //{
        //    string rest = "";
        //    List<ProductVariantResult> result = new List<ProductVariantResult>();

        //    try
        //    {

        //        rest = getAddress("/REST/AddVariants");

        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(variants));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            result = JsonConvert.DeserializeObject<List<ProductVariantResult>>(reader.ReadToEnd());
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return result;
        //}

        //public List<ProductVariant> GetProductVariantList()
        //{
        //    string rest = "";
        //    List<ProductVariant> lst = new List<ProductVariant>();

        //    try
        //    {
        //        rest = getAddress("/REST/GetProductVariantList");

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        lst = JsonConvert.DeserializeObject<List<ProductVariant>>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return lst;
        //}

        //public List<ProductVariant> GetModelsVariantList(string model)
        //{
        //    string rest = "";
        //    List<ProductVariant> lst = new List<ProductVariant>();

        //    try
        //    {
        //        rest = getAddress("/REST/GetModelsVariantList") + "/" + model;

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        lst = JsonConvert.DeserializeObject<List<ProductVariant>>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return lst;
        //}

        //public List<ProductDimension> GetModelsDimensionList(string model)
        //{
        //    string rest = "";
        //    List<ProductDimension> lst = new List<ProductDimension>();

        //    try
        //    {
        //        rest = getAddress("/REST/GetModelsDimensionList") + "/" + model;

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        lst = JsonConvert.DeserializeObject<List<ProductDimension>>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return lst;
        //}


        //public List<ProductDimensionResult> AddDimensions(List<GIS_Dto.ProductDimension> dimensions)
        //{
        //    string rest = "";
        //    List<ProductDimensionResult> result = new List<ProductDimensionResult>();

        //    try
        //    {

        //        rest = getAddress("/REST/AddDimensions");

        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(dimensions));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            result = JsonConvert.DeserializeObject<List<ProductDimensionResult>>(reader.ReadToEnd());
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return result;
        //}

        //public List<GIS_Dto.ProductDimension> GetProductDimensionList()
        //{
        //    string rest = "";
        //    List<GIS_Dto.ProductDimension> lst = new List<GIS_Dto.ProductDimension>();

        //    try
        //    {
        //        rest = getAddress("/REST/GetProductDimensionList");

        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        lst = JsonConvert.DeserializeObject<List<GIS_Dto.ProductDimension>>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return lst;
        //}


        //public InventoryResult ProductInventory(string productNo, string warehouseNo, string date, string amount)
        //{
        //    //string token, string productno, string warehouse, string placeno, string placename, string date, string time, string terminalid, string note, string amount
        //    string rest = "";

        //    try
        //    {

        //        rest = getAddress("/REST/InventoryProduct") + "/" + productNo + "/" + warehouseNo + "/" + "*" + "/" + "*" + "/" + date + "/" + "*" + "/" + "*" + "/" + "*" + "/" + amount;

        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "GET";
        //        request.ContentType = "application/json; charset=utf-8";

        //        using (var response = request.GetResponse())
        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            InventoryResult result = JsonConvert.DeserializeObject<InventoryResult>(reader.ReadToEnd());
        //            return result;
        //        }


        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //        return null;
        //    }
        //}

        public InventoryResult ExecuteInventory(InventoryParam param)
        {
            InventoryResult result = new InventoryResult();
            string rest = "";

            try
            {

                rest = getAddress("/REST/ExecuteInventory");

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = JsonConvert.DeserializeObject<InventoryResult>(reader.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }


        //public ProductBalancingResult ProductBalancing(string token, ProductBalancingParam param)
        //{
        //    string rest = "";

        //    try
        //    {
        //        ProductBalancingResult pbr = new ProductBalancingResult();
        //        rest = getAddress("/REST/ProductBalancing");

        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";

        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
        //            request.Headers.Add("Authorization", "Basic " + encoded);
        //        }

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(param));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            pbr = JsonConvert.DeserializeObject<ProductBalancingResult>(reader.ReadToEnd());
        //            return pbr;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //        return null;
        //    }
        //}


        private string getAddress(string rest_function)
        {
            string result = "";

            return result;

        }
    }
}
