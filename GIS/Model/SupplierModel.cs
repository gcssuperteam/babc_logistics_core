﻿using BABC_Logistics_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.Model
{
    public class SupplierModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SupplierModel(IConfiguration config) : base(config)
        {

        }
        public Supplier GetSupplierById(string id)
        {
            string rest = "";
            Supplier supplier = new Supplier();

            rest = GISBaseAddress.TrimEnd('/') + "/SupplierSvc/REST/GetSupplier/" + GISToken + "/" + id;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                supplier = JsonConvert.DeserializeObject<Supplier>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return supplier;
        }

        public List<Supplier> getSupplierList(SupplierListParam param)
        {
            string rest = "";
            List<Supplier> supplierList = new List<Supplier>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/SupplierSvc/GetSupplierList" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        supplierList = JsonConvert.DeserializeObject<List<Supplier>>(result);
                    }
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return supplierList;
        }
    }
}
