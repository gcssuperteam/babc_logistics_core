﻿using BABC_Logistics_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.Model
{
    public class ProductionModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ProductionModel(IConfiguration config) : base(config)
        {

        }
        public ProductionOrderHead GetMaterialOrder(string onr, string rdc)
        {
            string rest = "";
            ProductionOrderHead result = new ProductionOrderHead();
            rest = GISBaseAddress.TrimEnd('/') + "/ProductionSvc/REST/GetMaterialOrder/" + GISToken  + "/" + onr + "/" + rdc;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonSerializer.Deserialize<ProductionOrderHead>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;

        }
        public List<MaterialOrderHead> GetMaterialOrderList(MaterialOrderListParam param)
        {
            string rest = "";
            List<MaterialOrderHead> result = new List<MaterialOrderHead>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/ProductionSvc/REST/GetMaterialOrderList" + "/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(param));
                }
                using (var response = request.GetResponse())
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string resultString = reader.ReadToEnd();
                    result = JsonSerializer.Deserialize<List<MaterialOrderHead>>(resultString);
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public ProductionOrderHead GetProductionOrder(string onr, string rdc)
        {
            string rest = "";
            ProductionOrderHead result = new ProductionOrderHead();

            rest = GISBaseAddress.TrimEnd('/') + "/ProductionSvc/REST/GetProductionOrder/" + GISToken + "/" + onr + "/" + rdc;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonSerializer.Deserialize<ProductionOrderHead>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }



            return result;
        }

        public void UpdateMaterialRow(MaterialRow mr)
        {
            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/ProductionSvc/REST/UpdateMaterialRow/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(mr));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }
    }
}
