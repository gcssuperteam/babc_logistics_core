﻿using BABC_Logistics_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.Model
{
    public class UserModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public UserModel(IConfiguration config) : base(config)
        {

        }

        public string Login(string userid, string password)
        {
            string rest = "";
            string token = "";


            try
            {

                if (!string.IsNullOrEmpty(userid) && !string.IsNullOrEmpty(password))
                {
                    rest = GISBaseAddress.TrimEnd('/') +  "/Login/REST/LoginToken/" + GISToken + "/" + userid + "/" + password;
                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);

                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    try
                    {
                        token = response.ToString();
                    }
                    catch (Exception e)
                    {
                        mLog.Error("Login failed: " + rest, e);
                    }
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return token;
        }

        public string Logoff(string userid)
        {
            string rest = "";
            string token = "";

            try
            {

                if (!string.IsNullOrEmpty(userid))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/Login/REST/LogOffToken/" + userid;
                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);

                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    try
                    {
                        token = response.ToString();
                    }
                    catch (Exception e)
                    {
                        mLog.Error("Logoff failed: " + rest, e);
                    }
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return token;
        }
        public User GetUser(string uid, string username)
        {
            string rest = "";
            User user = null;

            try
            {

                if (!string.IsNullOrEmpty(username))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/UserSvc/REST/GetUser/" + GISToken + "/" + username;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);

                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    try
                    {
                        user = JsonConvert.DeserializeObject<User>(response);
                    }
                    catch (Exception e)
                    {
                        mLog.Error("Login failed: " + rest, e);
                    }
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return user;
        }
    }
}
