﻿using BABC_Logistics_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.Model
{
    public class PurchaseModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PurchaseModel(IConfiguration config) : base(config)
        {

        }

        public string AddOrderHead(PurchaseOrderHead oh)
        {
            string rest = "", result = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/AddOrderHead/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(oh));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public PurchaseOrderHead GetOrderById(string id, bool with_rows)
        {
            string rest = "";
            PurchaseOrderHead oh = null;

            try
            {

                if (!string.IsNullOrEmpty(id))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/GetOrderById/" + GISToken + @"/" + id + @"/" + with_rows.ToString();

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    oh = JsonConvert.DeserializeObject<PurchaseOrderHead>(response);
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return oh;
        }

        public List<PurchaseOrderHead> GetOrderByIdxList(string idx, string idxfilter, string filter, bool with_rows, int? max_count, bool reverse_reading)
        {
            string rest = "";

            List<PurchaseOrderHead> lst = new List<PurchaseOrderHead>();

            if (max_count == null)
                max_count = 0;

            try
            {
                if (!string.IsNullOrEmpty(idx))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/GetOrderByIdxList/" + GISToken + @"/" + idx + @"/" + idxfilter + @"/" + filter + @"/" + with_rows.ToString() + @"/" + max_count + @"/" + reverse_reading.ToString();

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonConvert.DeserializeObject<List<PurchaseOrderHead>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<PurchaseOrderHead> GetOrderPickList(string idx, string idxfilter, string idxCount, string filter, string orderserie, bool with_rows, bool includeDeliveredRows, int? max_count, bool reverse_reading)
        {
            string rest = "";

            List<PurchaseOrderHead> lst = new List<PurchaseOrderHead>();

            if (max_count == null)
                max_count = 0;

            try
            {
                if (!string.IsNullOrEmpty(idx))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/GetOrderPickList/" + GISToken + @"/" + idx + @"/" + idxfilter + @"/" + idxCount + @"/" + filter + @"/" + orderserie + @"/" + with_rows.ToString() + @"/" + includeDeliveredRows.ToString() + @"/" + max_count + @"/" + reverse_reading.ToString();

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);

                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonConvert.DeserializeObject<List<PurchaseOrderHead>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }
        public void UpdateOrderHead(PurchaseOrderHead oh)
        {
            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/UpdateOrderHead/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(oh));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

        public DeliverResult Deliver(List<PurchaseDeliverRowParam> rows)
        {
            string rest = "";
            DeliverResult deliverresult = null;

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/DeliverPurchaseOrder/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(rows));
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        deliverresult = JsonConvert.DeserializeObject<DeliverResult>(result);
                    }
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return deliverresult;
        }
        public OrderRow GetOrderRow(string orderid, string rowid)
        {
            string rest = "";
            OrderRow oh = null;

            try
            {
                if (!string.IsNullOrEmpty(orderid) && !string.IsNullOrEmpty(rowid))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/GetOrderRow/" + GISToken + "/" + orderid + "/" + rowid;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    oh = JsonConvert.DeserializeObject<OrderRow>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return oh;
        }
        public DeliverResult BackDeliver(List<BackDeliverRowParam> rows)
        {
            string rest = "";
            DeliverResult deliverresult = null;

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/BackDeliverOnPurchaseOrder/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(rows));
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        deliverresult = JsonConvert.DeserializeObject<DeliverResult>(result);
                    }
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return deliverresult;
        }

        public PurchaseOrderHead GetOrderForDeliverNote(string delivernote)
        {
            string rest = "";
            PurchaseOrderHead oh = null;

            try
            {

                if (!string.IsNullOrEmpty(delivernote))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/GetOrderForDeliverNote/" + GISToken + @"/" + delivernote;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    oh = JsonConvert.DeserializeObject<PurchaseOrderHead>(response);
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return oh;
        }
        public bool updateReceiptSignature(string receiptnote, string sign)
        {
            string rest = "";
            try
            {

                if (!string.IsNullOrEmpty(receiptnote))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/updateReceiptSignature/" + GISToken + @"/" + receiptnote + @"/" + sign;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    return JsonConvert.DeserializeObject<bool>(response);
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);

            }

            return false;
        }

        public void UpdateTransport(Transport transport)
        {
            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/UpdateTransport/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(transport));
                }
                using (var response = request.GetResponse())
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

        }

        public List<OrderRow> GetOrderRowList(string filter)
        {
            string rest = "";

            List<OrderRow> lst = new List<OrderRow>();

            try
            {
                if (!string.IsNullOrEmpty(filter))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/GetOrderRowList/" + GISToken + filter;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonConvert.DeserializeObject<List<OrderRow>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public void UpdateOrderRowList(List<OrderRow> lst)
        {
            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/PurchaseOrderSvc/REST/UpdateOrderRowList/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(lst));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

    }
}
