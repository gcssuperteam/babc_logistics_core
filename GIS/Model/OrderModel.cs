﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BABC_Logistics_core.GIS.DTO;
using BABC_Logistics_core.GIS.Model;
using System.Text.Json;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace BABC_Logistics_core.GIS.Model
{
    public class OrderModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public OrderModel(IConfiguration config) : base(config)
        {
            
        }

        public OrderHead GetOrderById(string id, bool with_rows)
        {
            string rest = "";
            OrderHead oh = null;

            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderById/" + GISToken + "/" + id + @"/" + with_rows.ToString();

                    //mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    oh = JsonSerializer.Deserialize<OrderHead>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return oh;
        }

        public List<OrderHead> GetOrderByIdxList(string idx, string idxfilter, string filter, bool with_rows, int? max_count, bool reverse_reading)
        {
            string rest = "";

            List<OrderHead> lst = new List<OrderHead>();

            if (max_count == null)
                max_count = 0;

            try
            {
                if (!string.IsNullOrEmpty(idx))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderByIdxList/" + GISToken + @"/" + idx + @"/" + idxfilter + @"/" + filter + @"/" + with_rows.ToString() + @"/" + max_count + @"/" + reverse_reading.ToString();

                    //mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    request.Timeout = 3600000;

                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonSerializer.Deserialize<List<OrderHead>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<OrderHead> GetOrderPickList(string idx, string idxfilter, string idxCount, string filter, string orderserie, bool with_rows, bool includeDeliveredRows, int? max_count, bool reverse_reading)
        {
            string rest = "";

            List<OrderHead> lst = new List<OrderHead>();

            if (max_count == null)
                max_count = 0;

            try
            {
                if (!string.IsNullOrEmpty(idx))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderPickList/" + GISToken + @"/" + idx + @"/" + idxfilter + @"/" + idxCount + @"/" + filter + @"/" + orderserie + @"/" + with_rows.ToString() + @"/" + includeDeliveredRows.ToString() + @"/" + max_count + @"/" + reverse_reading.ToString();

                    //mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);

                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonSerializer.Deserialize<List<OrderHead>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }
        public List<OrderHead> GetOrderPickListWithAdditionalOrders(string idx, string idxfilter, string idxCount, string filter, string orderserie, string with_rows, string includeDeliveredRows, int? max_count, string reverse_reading, string additional_filter)
        {
            string rest = "";

            List<OrderHead> lst = new List<OrderHead>();

            if (max_count == null)
                max_count = 0;

            try
            {
                if (!string.IsNullOrEmpty(idx))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderPickListWithAdditionalOrders/" + GISToken + @"/" + idx + @"/" + idxfilter + @"/" + idxCount + @"/" + filter + @"/" + orderserie + @"/" + with_rows.ToString() + @"/" + includeDeliveredRows.ToString() + @"/" + max_count + @"/" + reverse_reading.ToString() + @"/" + additional_filter;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    request.Timeout = 3600000;
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);

                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonSerializer.Deserialize<List<OrderHead>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }


        //public List<OrderHead> GetOrderPickListFromCache(OrderCascheQuery query)
        //{
        //    string rest = "";
        //    string sql_filter_string;

        //    List<OrderHead> lst = new List<OrderHead>();

        //    try
        //    {
        //        rest = getAddress("/REST/GetOrderPickListFromCache");
        //        mLog.Debug("CALL TO: " + rest);

        //        WebRequest request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";
        //        Encoding enc = System.Text.Encoding.GetEncoding(1252);

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(query));
        //        }

        //        using (var response = request.GetResponse())
        //        {
        //            using (var reader = new StreamReader(response.GetResponseStream()))
        //            {
        //                string result = reader.ReadToEnd();
        //                lst = JsonConvert.DeserializeObject<List<OrderHead>>(result);
        //            }
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return lst;

        //}

        public List<OrderRow> GetOrderRowByOrderIdList(string idx)
        {
            string rest = "";

            List<OrderRow> lst = new List<OrderRow>();

            try
            {
                if (!string.IsNullOrEmpty(idx))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderRowByOrderIdList/" + GISToken + @"/" + idx;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonSerializer.Deserialize<List<OrderRow>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<OrderRow> GetOrderRowList(short index, string indexfilter, string general_filter_string, bool with_rows, int max_count, bool reverse_reading)
        {
            string rest = "";

            List<OrderRow> lst = new List<OrderRow>();

            try
            {
                if (!string.IsNullOrEmpty(indexfilter))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderRowList/" + GISToken + @"/" + index + @"/" + indexfilter + @"/" + general_filter_string + @"/" + with_rows + @"/" + max_count + @"/" + reverse_reading;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    request.Timeout = 720000;
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonSerializer.Deserialize<List<OrderRow>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<OrderRow> GetOrderRowByProductList(string orderno, string productno)
        {
            string rest = "";

            List<OrderRow> lst = new List<OrderRow>();

            try
            {
                if (!string.IsNullOrEmpty(orderno) && !string.IsNullOrEmpty(productno))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderRowByProductList/" + GISToken + @"/" + orderno + @"/" + productno;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonSerializer.Deserialize<List<OrderRow>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<OrderRow> GetOrderRowByProductId(string productno, string filter)
        {
            string rest = "";

            List<OrderRow> lst = new List<OrderRow>();

            try
            {
                if (!string.IsNullOrEmpty(productno))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderRowByProductId/" + GISToken + @"/" + productno + @"/" + filter;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonSerializer.Deserialize<List<OrderRow>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public OrderRow GetOrderRow(string orderid, string rowid)
        {
            string rest = "";
            OrderRow oh = null;

            try
            {
                if (!string.IsNullOrEmpty(orderid) && !string.IsNullOrEmpty(rowid))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderRow/" + GISToken + "/" + orderid + "/" + rowid;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    oh = JsonSerializer.Deserialize<OrderRow>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return oh;
        }

        public List<OrderRowText> GetOrderRowTextList(string orderid, string rowid)
        {
            string rest = "";
            List<OrderRowText> lst = new List<OrderRowText>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderRowTextList/" + GISToken + "/" + orderid + "/" + rowid;

                WebRequest request = WebRequest.Create(rest);
                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonSerializer.Deserialize<List<OrderRowText>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
            return lst;
        }

        public string AddOrderHead(OrderHead oh)
        {
            string rest = "", result = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/AddOrderHead/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(oh));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public void UpdateOrderHead(OrderHead oh)
        {
            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/UpdateOrderHead/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(oh));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

        //public OrderResult<string> UpdateOrderRow(OrderRow or)
        //{
        //    string rest = "";
        //    OrderResult<string> orderresult = new OrderResult<string>();

        //    try
        //    {
        //        rest = getAddress("/REST/UpdateOrderRow");
        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(or));
        //        }

        //        using (var response = request.GetResponse())
        //        {
        //            using (var reader = new StreamReader(response.GetResponseStream()))
        //            {
        //                string result = reader.ReadToEnd();
        //                orderresult = JsonConvert.DeserializeObject<OrderResult<string>>(result);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return orderresult;
        //}

        //public OrderResult<string> DeleteOrderRow(OrderRow or)
        //{
        //    string rest = "";
        //    OrderResult<string> orderresult = new OrderResult<string>();

        //    try
        //    {
        //        rest = getAddress("/REST/DeleteOrderRow");
        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(or));
        //        }

        //        using (var response = request.GetResponse())
        //        {
        //            using (var reader = new StreamReader(response.GetResponseStream()))
        //            {
        //                string result = reader.ReadToEnd();
        //                orderresult = JsonConvert.DeserializeObject<OrderResult<string>>(result);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return orderresult;
        //}

        //public void DeleteOrderHead(OrderHead oh)
        //{
        //    string rest = "";
        //    try
        //    {
        //        rest = getAddress("/REST/DeleteOrderHead");
        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(oh));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            string result = reader.ReadToEnd();
        //            // do something with the results
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }
        //}

        public void UpdateOrderRowList(List<OrderRow> lst)
        {
            string rest = "";
            try
            {
                //rest = getAddress("/REST/UpdateOrderRowList");
                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/UpdateOrderRowList/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    //writer.Write(JsonConvert.SerializeObject(lst));
                    writer.Write(JsonSerializer.Serialize(lst));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

        //public void DeleteOrderRowList(List<OrderRow> lst)
        //{
        //    string rest = "";
        //    try
        //    {
        //        rest = getAddress("/REST/DeleteOrderRowList");
        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(lst));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            string result = reader.ReadToEnd();
        //            // do something with the results
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }
        //}

        //public void UpdateOrderRowTextList(List<OrderRowText> lst)
        //{
        //    string rest = "";
        //    try
        //    {
        //        rest = getAddress("/REST/UpdateOrderRowTextList");
        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(lst));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            string result = reader.ReadToEnd();
        //            // do something with the results
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }
        //}

        //public void DeleteOrderRowTextList(List<OrderRowText> lst)
        //{
        //    string rest = "";

        //    try
        //    {
        //        rest = getAddress("/REST/DeleteOrderRowTextList");
        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(lst));
        //        }

        //        using (var response = request.GetResponse())

        //        using (var reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            string result = reader.ReadToEnd();
        //            // do something with the results
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }
        //}

        //[Obsolete("Use Deliver and BackDeliver methods instead")]
        //public DeliverResult DeliverOrderRows(List<OrderRow> rows, bool back)
        //{
        //    string rest = "";
        //    DeliverResult deliverresult = null;

        //    try
        //    {
        //        rest = getAddress("/REST/DeliverOrderRows") + "/" + back.ToString();
        //        mLog.Debug("CALL TO: " + rest);

        //        var request = WebRequest.Create(rest);
        //        request.Method = "POST";
        //        request.ContentType = "application/json; charset=utf-8";

        //        using (var writer = new StreamWriter(request.GetRequestStream()))
        //        {
        //            writer.Write(JsonConvert.SerializeObject(rows));
        //        }

        //        using (var response = request.GetResponse())
        //        {
        //            using (var reader = new StreamReader(response.GetResponseStream()))
        //            {
        //                string result = reader.ReadToEnd();
        //                deliverresult = JsonConvert.DeserializeObject<DeliverResult>(result);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return deliverresult;
        //}

        public DeliverResult Deliver(List<DeliverRowParam> rows)
        {
            string rest = "";
            DeliverResult deliverresult = null;

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/Deliver/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(rows));
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        deliverresult = JsonSerializer.Deserialize<DeliverResult>(result);
                    }
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return deliverresult;
        }

        public DeliverResult BackDeliver(List<BackDeliverRowParam> rows)
        {
            string rest = "";
            DeliverResult deliverresult = null;

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/BackDeliver/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(rows));
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        deliverresult = JsonSerializer.Deserialize<DeliverResult>(result);
                    }
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return deliverresult;
        }

        public Transport GetTransportForDeliverNote(string delivernote)
        {
            string rest = "";
            Transport transport = null;

            try
            {

                if (!string.IsNullOrEmpty(delivernote))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetTransportForDeliverNote/" + GISToken + @"/" + delivernote;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    transport = JsonSerializer.Deserialize<Transport>(response);
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return transport;
        }

        public OrderHead GetOrderForDeliverNote(string delivernote)
        {
            string rest = "";
            OrderHead oh = null;

            try
            {

                if (!string.IsNullOrEmpty(delivernote))
                {
                    rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderForDeliverNote/" + GISToken + @"/" + delivernote;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    oh = JsonSerializer.Deserialize<OrderHead>(response);
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return oh;
        }

        public List<OrderHead> GetOrderForDeliverNoteList(string uid, int idxCount, string filter, int max_count, bool reverse_reading)
        {
            string rest = "";
            List<OrderHead> lst = null;

            try
            {

                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/GetOrderForDeliverNoteList/" + GISToken + @"/" + idxCount.ToString() + @"/" + filter + @"/" + max_count.ToString() + @"/" + reverse_reading.ToString();

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonSerializer.Deserialize<List<OrderHead>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public void AddTransport(Transport transport)
        {
            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/AddTransport/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(transport));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

        }
        //public OrderHeadFreight getOrderHeadFreightForOrder(string orderno)
        //{
        //    string rest = "";
        //    OrderHeadFreight ohf = null;

        //    try
        //    {

        //        if (!string.IsNullOrEmpty(orderno))
        //        {
        //            rest = getAddress("/REST/GetOrderHeadFreightForOrder") + @"/" + orderno;

        //            mLog.Debug("CALL TO: " + rest);

        //            WebRequest request = WebRequest.Create(rest);
        //            WebResponse ws = request.GetResponse();
        //            Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //            StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //            string response = responseStream.ReadToEnd();
        //            responseStream.Close();

        //            ohf = JsonSerializer.Deserialize<OrderHeadFreight>(response);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        mLog.Error("Error in call: " + rest, e);
        //    }

        //    return ohf;
        //}



        public void UpdateTransport(Transport transport)
        {
            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/UpdateTransport/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(transport));
                }
                using (var response = request.GetResponse())
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

        }
        public List<OrderHead> getOrderPickListFromCache(OrderListParam param)
        {
            string rest = "";
            List<OrderHead> lst = new List<OrderHead>();
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/OrderSvc/REST/getOrderList" + "/" + base.GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(param));
                }
                using (var response = request.GetResponse())
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    lst = JsonSerializer.Deserialize<List<OrderHead>>(result);
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
            return lst;

        }
    }
}
