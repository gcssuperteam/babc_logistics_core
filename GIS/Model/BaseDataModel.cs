﻿using BABC_Logistics_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.Model
{
    public class BaseDataModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string endpointAddress = "BaseDataSvc";

        public BaseDataModel(IConfiguration config) : base(config)
        {

        }

        public List<BaseTable> GetBaseTableList(string type)
        {
            string rest = "";
            List<BaseTable> lst = new List<BaseTable>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/" + endpointAddress + "/REST/GetBaseTableList/" + GISToken + "/" + type;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonSerializer.Deserialize<List<BaseTable>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public BaseTable GetBaseTable(string type, string key)
        {
            string rest = "";
            BaseTable bt = new BaseTable();

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(key))
                return null;

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/REST/GetBaseTable/" + GISToken + "/" + type + "/" + key;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                bt = JsonSerializer.Deserialize<BaseTable>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return null;
            }

            return bt;
        }

        public string AddBaseTable(BaseTable bd)
        {
            string rest = "";

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/REST/AddBaseTable/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(bd));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return "false";
            }
            return "true";
        }

        public void UpdateBaseTable(BaseTable bd)
        {
            string rest = "";

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/REST/UpdateBaseTable/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(bd));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

        public void DeleteBaseTable(BaseTable bd)
        {
            string rest = "";

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/REST/DeleteBaseTable/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(bd));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

        private string getAddress(string rest_function)
        {
            string result = "";

            return result;
        }
    }
}
