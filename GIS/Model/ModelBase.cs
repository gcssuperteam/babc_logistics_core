﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.Model
{
    public class ModelBase
    {
        public string GISBaseAddress { get; set; }
        public string GISToken { get; set; }

        private readonly IConfiguration Configuration;

        public ModelBase(IConfiguration configuration)
        {
            Configuration = configuration;

            try
            {
                GISBaseAddress =  Configuration.GetValue<string>("GIS:BaseAddress");
                GISToken = Configuration.GetValue<string>("GIS:Token");

            }
            catch (Exception e)
            {

            }

        }

        public ModelBase()
        {
        }

        private string getAddress()
        {
            string result = "";

            return result;
        }
    }
}
