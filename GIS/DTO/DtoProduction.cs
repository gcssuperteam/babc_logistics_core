﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.DTO
{
    public partial class MaterialOrderHead
    {
        public string OrderNo { get; set; }
        public string RowNo { get; set; }
        public string Date { get; set; }
        public string OrderType { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public decimal Amount { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string CompanyId { get; set; }
        public string DeliverState { get; set; }
        public string ProdPlanState { get; set; }
        public string MaterialPlanState { get; set; } // MTF
        public string DeliverableState { get; set; }  // BLF
        public List<MaterialOrderRow> MaterialOrderRows { get; set; }
        public List<MaterialRow> MaterialRows { get; set; }
    }

    public partial class MaterialOrderRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public decimal Amount { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string MaterialPlanFlag { get; set; }
        public string MaterialDocumentFlag { get; set; }
        public bool FullyDeliverable { get; set; }
        public Product Product { get; set; }
        public List<MaterialRow> MaterialRows { get; set; }
        public List<OperationRow> OperationRows { get; set; }

    }

    public partial class MaterialRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public int MaterialRowNo { get; set; }
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public string Unit { get; set; }
        public decimal? Amount { get; set; }
        public decimal? DeliveredAmount { get; set; }
        public string DeliverState { get; set; }
        public string PreferedDeliverDate { get; set; }
        public bool FullyDeliverable { get; set; }
        public Product Product { get; set; }
        public string NoteStructure { get; set; }
        public string PositionNumber { get; set; }
        public int? NumberDecimal { get; set; }
        public string OperationNo { get; set; }
        public string ReservationCode { get; set; }
    }

    public partial class OperationRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public int OperationRowNo { get; set; }
        public string OperationNo { get; set; }
        public string OperationType { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string ProductionGroup { get; set; }
        public string Priority { get; set; }
        public string StartDate { get; set; }
        public string StopDate { get; set; }
        public decimal? PlannedAmount { get; set; }
        public decimal? ReportedTimeMan { get; set; }
        public decimal? ReportedTimeMachine { get; set; }
        public string Status { get; set; }
        public ProductionGroup ProductionGroupObject { get; set; }
    }

    public partial class ProductionGroup
    {
        public string Group { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string PartOfGroup { get; set; }
        public string CostPlace { get; set; }
        public string ManufactoringYear { get; set; }
        public string AcquisitionYear { get; set; }
        public string DiscardYear { get; set; }
        public string LastService { get; set; }
        public string ServiceDoneBy { get; set; }
        public string NextService { get; set; }
        public string Condition { get; set; }
        public string Type { get; set; }
        public string SupplierNo { get; set; }
        public string OccupancyKey { get; set; }
        public decimal? QueuingTimeBefore { get; set; }
        public decimal? QueuingTimeAfter { get; set; }
        public decimal? AmountOfMachines { get; set; }
        public decimal? OccupancyFactor { get; set; }
        public decimal? CapacityFactor { get; set; }
        public decimal? HourlyCostFixed { get; set; }
        public decimal? HourlyCostVariable { get; set; }
        public decimal? AcquisitionCost { get; set; }
        public decimal? CalculatedValue { get; set; }
        public string ProjectAccount { get; set; }
        public string WarehouseNo { get; set; }
        public string PermissionRequriment { get; set; }
        public string AccountInternalSale { get; set; }
    }

    public partial class ProductionOrderHead
    {
        public string OrderNo { get; set; }
        public string RowNo { get; set; }
        public string Date { get; set; }
        public string OrderType { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public decimal Amount { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string CompanyId { get; set; }
        public string DeliverState { get; set; }
        public string ProdPlanState { get; set; }
        public string MaterialPlanState { get; set; } // MTF
        public string DeliverableState { get; set; }  // BLF
        public string Unit { get; set; }  // BLF
        public List<OperationRow> OperationRows { get; set; }
        public List<MaterialRow> MaterialRows { get; set; }
    }
}
