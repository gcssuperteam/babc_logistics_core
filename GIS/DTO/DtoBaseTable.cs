﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.DTO
{
    public class SellerDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ExtName { get; set; }
        public string CostPlace { get; set; }
        public string UserId { get; set; }
        public string EmployeNo { get; set; }
    }

    public partial class BaseTable
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string Type { get; set; }
        public string Code1 { get; set; }
        public string Code2 { get; set; }
        public string Code3 { get; set; }
        public string Code4 { get; set; }
        public string Code5 { get; set; }
        public string Code6 { get; set; }
        public string Code7 { get; set; }
        public string Code8 { get; set; }
        public string Code9 { get; set; }
        public string Code10 { get; set; }
        public string Code11 { get; set; }
        public string Code12 { get; set; }
        public int? Num1 { get; set; }
        public int? Num2 { get; set; }
        public int? Num3 { get; set; }
        public int? Num4 { get; set; }
        public int? Num5 { get; set; }
        public int? Num6 { get; set; }
        public int? Num7 { get; set; }
        public int? Num8 { get; set; }
        public int? Num9 { get; set; }
        public int? Num10 { get; set; }
        public int? Num11 { get; set; }
        public int? Num12 { get; set; }
    }
}
