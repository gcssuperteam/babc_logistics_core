﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.DTO
{
    public partial class DeliverResult
    {
        public bool Succeeded { get; set; }
        public string OrderNo { get; set; }
        public string DeliverNo { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string InternalDeliverMessage { get; set; }
        public string OHDeliverState { get; set; }

        public List<OrderRow> DeliveredRows { get; set; }
    }

    public partial class BackDeliverRowParam
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string DeliverNote { get; set; }
        public bool BackDeliverOnMaterialRow { get; set; }
    }

    public partial class DeliverRowParam
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string WarehouseNo { get; set; }
        public string DeliverState { get; set; }
        public decimal AmountToDeliver { get; set; }
        public string DeliverNote { get; set; }
        public bool DeliverMaterialRow { get; set; }
        public bool HandleOperationRow { get; set; }
        public string DeliverDate { get; set; }
    }

    public partial class MaterialOrderListParam
    {
        public short Index { get; set; }
        public string IndexFilter { get; set; }
        public string Filter { get; set; }
        public string DeliverDateFrom { get; set; }
        public string DeliverDateTo { get; set; }
        public bool WithRows { get; set; }
        public int MaxCount { get; set; }
        public bool ReverseReading { get; set; }
        public bool WithProduct { get; set; }
        public bool OnlyStarteble { get; set; }
        public CacheParam Cache { get; set; }
    }

    public class CacheParam
    {
        public string CacheId { get; set; }
        public bool UpdateTo { get; set; }
        public bool ReadFrom { get; set; }
        public int ExpirationInMinutes { get; set; }
        public bool UseInMemoryCache { get; set; }
        public bool CheckConsistency { get; set; }
    }

    public partial class OrderListParam
    {
        public short Index { get; set; }
        public string IndexFilter { get; set; }
        public string OrderSerie { get; set; }
        public string Filter { get; set; }
        public bool WithRows { get; set; }
        public int MaxCount { get; set; }
        public bool ReverseReading { get; set; }
        public bool IncludeDeliveredRows { get; set; }
        public bool ReadByFile { get; set; }
        public CacheParam Cache { get; set; }
    }

    public partial class PickListParam
    {
        public short Index { get; set; }
        public string IndexFilter { get; set; }
        public int IndexCount { get; set; }
        public string OrderSerie { get; set; }
        public string Filter { get; set; }
        public string AdditionalFilter { get; set; }
        public string OGRFilter { get; set; }
        public bool WithRows { get; set; }
        public int MaxCount { get; set; }
        public bool ReverseReading { get; set; }
        public bool IncludeDeliveredRows { get; set; }
        public bool ExcludeNonWorkingDays { get; set; }
        public bool IncludeDeliverCustomer { get; set; }
        public bool ReadByFile { get; set; }
        public CacheParam Cache { get; set; }
    }

    public class FilterDTO
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Filter { get; set; }
        public bool UseCache { get; set; }
        public string CacheName { get; set; }
    }

    public partial class InventoryResult
    {
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string PlaceNo { get; set; }
        public string PlaceName { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Terminal { get; set; }
        public string Note { get; set; }
        public decimal OldAmount { get; set; }
        public decimal NewAmount { get; set; }
        public decimal Amount { get; set; }
        public string InternalMoveMessage { get; set; }
        public bool Succeeded { get; set; }
    }

    public partial class InventoryParam
    {
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string PlaceNo { get; set; }
        public string PlaceName { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Terminal { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
    }

    public partial class MoveParam
    {
        public string ProductNo { get; set; }
        public string MoveFrom { get; set; }
        public string MoveTo { get; set; }
        public decimal Amount { get; set; }
    }

    public partial class MoveResult
    {
        public string ProductNo { get; set; }
        public string MoveFrom { get; set; }
        public string MoveTo { get; set; }
        public decimal Amount { get; set; }
        public string InternalMoveMessage { get; set; }
        public bool Succeeded { get; set; }
    }

    public partial class SupplierListParam
    {
        public short Index { get; set; }
        public string IndexFilter { get; set; }
        public string Filter { get; set; }
        public string GetIbanFromField { get; set; }
        public string GetVatFromField { get; set; }
        public int MaxCount { get; set; }
    }

    public partial class PurchaseDeliverRowParam
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public decimal Amount { get; set; }
        public string WarehouseNo { get; set; }
        public string DeliverState { get; set; }
        public string DeliverNoteNo { get; set; }
        public string DeliverDate { get; set; }
    }
}
