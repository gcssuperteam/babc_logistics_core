﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BABC_Logistics_core.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Inventory",
                columns: table => new
                {
                    ID = table.Column<string>(nullable: false),
                    ProductNo = table.Column<string>(nullable: true),
                    WarehouseNo = table.Column<string>(nullable: true),
                    Date = table.Column<string>(nullable: true),
                    Time = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    OriginalAmount = table.Column<decimal>(nullable: false),
                    AmountAtTime = table.Column<DateTime>(nullable: false),
                    VIPAtTime = table.Column<decimal>(nullable: false),
                    InventoryDone = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventory", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Inventory");
        }
    }
}
