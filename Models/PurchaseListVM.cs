﻿using BABC_Logistics_core.GIS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.Models
{
    public class PurchaseListVM
    {
        public List<PurchaseOrderHead> OrderHeads { get; set; }
        public string search_order { get; set; }
        public string search_supplierName { get; set; }
    }
}
