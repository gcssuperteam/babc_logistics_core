﻿using BABC_Logistics_core.GIS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.Models
{
    public class MaterialPickVM
    {
        public string Sku { get; set; }
        public string OperationNo { get; set; }
        public ProductionOrderHead OH { get; set; }
        public List<MaterialRow> MaterialRowList { get; set; }
        public bool Succeeded { get; set; }
        public bool ShowMessage { get; set; }
        public string Message { get; set; }
        public string MessageHeader { get; set; }
        public ProductLabel Label { get; set; }
    }

    public class ProductLabel
    {
        public string LabelProductNo { get; set; }
        public string ProductNoBeingManufactured { get; set; }
    }
}
