﻿using BABC_Logistics_core.GIS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.Models
{
    public class OrderViewModel
    {
        public OrderHead Order;
        public List<Product> SelectableProducts;
        //public List<string> additionalOrders;
        //public List<MaterialRow> materials;
    }

    public class PurchaseOrderViewModel
    {
        public PurchaseOrderHead Order;
        public List<Product> SelectableProducts;
        //public List<string> additionalOrders;
        //public List<MaterialRow> materials;
    }
}
