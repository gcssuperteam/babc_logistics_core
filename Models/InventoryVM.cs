﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.Models
{
    public class InventoryVM
    {
        public string ProductNo { get; set; }
        public string Warehouse { get; set; }
        public string Message { get; set; }
        public decimal Amount { get; set; }
        public DateTime InventoryDate { get; set; }
        public List<InventoryDetail> InventoryWaitingForUpdate { get; set; }
    }

    public class InventorySumVM
    {
        public string ProductNo { get; set; }
        public string Warehouse { get; set; }
        public decimal Amount { get; set; }
        public string InventoryDate { get; set; }
        public List<InventoryDetail> ContainingInventories { get; set; }
    }

    public partial class InventoryDetail
    {
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
        public decimal OriginalAmount { get; set; }
        public DateTime AmountAtTime { get; set; }
        public decimal VIPAtTime { get; set; }
        public bool InventoryDone { get; set; }
    }
}
